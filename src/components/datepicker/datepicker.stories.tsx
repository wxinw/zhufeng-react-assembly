import React from "react";
import { Datepicker } from "./index";
import {
	withKnobs,
	text,
	boolean,
	color,
	select,
	number,
} from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

export default {
	title: "Datepicker",
	component: Datepicker,
	decorators: [withKnobs],
};

export const knobsBtn = () => (
	<Datepicker callback={action("callback")}
			delay={number("delay", 200)}
			initDate={text("initDate", "")} />
);